const Bot = require('node-telegram-bot');
const config = require('./config.js');

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

let bot = new Bot({
  token: config.token
}).on('inline_message', function(msg){
  if (msg.callback_query) {
    update(msg.callback_query);
  }
}).start();


let _options = {
  chat_id: '@knightmare_lands'
};

var _msg = {};
var _votes = {};

const emoji = {
  thumbsup: '\u{1F44D}',
  thumbsdown: '\u{1F44E}'
};

function send(text) {
  let options = Object.assign({}, _options, {
    text: text,
    parse_mode: 'Markdown',
    disable_web_page_preview: true,
    reply_markup: {
      inline_keyboard: [[{
        text: `${emoji.thumbsup} 0`,
        callback_data: 1
      },
      {
        text: `${emoji.thumbsdown} 0`,
        callback_data: -1
      }]]
    }
  });

  bot.sendMessage(options, (err, resp)=>{
    if (err) {
      console.log('Error: ', err);
      return;
    }
    _msg[resp.message_id] = text;
    _votes[resp.message_id] = {plus:[], minus:[]};
  });
}

function update(data){
  let msg_id = data.message.message_id;
  let user_id = data.from.id;

  let votes = _votes[msg_id];
  if (!votes) {
    votes = _votes[msg_id] = {plus:[], minus:[]};
  }

  let plus_index = votes.plus.indexOf(user_id);
  let minus_index = votes.minus.indexOf(user_id);

  if (plus_index > -1) {
    votes.plus.splice(plus_index, 1);
  }

  if (minus_index > -1) {
    votes.minus.splice(minus_index, 1);
  }

  let callback_data = parseInt(data.data, 10);
  let reply_text;

  if ((callback_data > 0 && plus_index > -1) ||
      (callback_data < 0 && minus_index > -1)) {
    reply_text = 'Your vote was removed';
  } else {
    votes[callback_data < 0 ? 'minus' : 'plus'].push(user_id);
    reply_text = `You ${callback_data < 0 ? 'disliked' : 'liked'} the post`;
  }

  let options = Object.assign({}, _options, {
    chat_id: data.message.chat.id,
    message_id: msg_id,
    reply_markup: {
      inline_keyboard: [[{
        text: `${emoji.thumbsup} ${votes.plus.length}`,
        callback_data: 1
      },
      {
        text: `${emoji.thumbsdown} ${votes.minus.length}`,
        callback_data: -1
      }]]
    }
  });

  bot.editMessageReplyMarkup(options, (err, resp)=>{
    let text;

    if (err) {
      text = 'Something went wrong, sorry';
    } else {
      text = reply_text;
    }

    bot.answerCallbackQuery({
      callback_query_id: data.id,
      text: text
    });
  });
}

const rxClear = /(^\s)|(\s+$)/;

function hookText(data) {
  let changes = data.commits.map(commit=>{
    let msg = commit.message.replace(rxClear, '');
    let id = commit.id;
    let id_short = id.slice(0, 8);
    return `[${id_short}](${commit.url}): ${msg}`;
  }).join('\n');

  return `Update for: [${data.repository.name}](${data.repository.homepage})\n${changes}`;
}

app.use(bodyParser.json());
app.post('/webhooks/gitlab', function(req, res){
  res.status(200).send('OK');

  let token = req.get('X-Gitlab-Token');
  if (token != config.gitlab) {
    return;
  }

  let data = req.body;
  send(hookText(data));
});

//send(`_I'm alive!_`);

app.listen(4010, 'localhost');

